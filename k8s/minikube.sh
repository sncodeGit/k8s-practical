#!/bin/bash

# Minikube - инструмент для запуска одноузлового кластера Kubernetes
# на виртуальной машине в персональном компьютере
minikube start

# kubectl - утилита для взаимодействия с k8s API
kubectl get nodes

# Создаем namespace
kubectl create namespace dev

#
kubectl apply -f python-server.deployment.yml

#
kubectl get pods -n dev

#
kubectl get deployment -n dev


#
kubectl port-forward -n dev pserver-<...> 8080:8080

#
kubectl delete deployment -n dev pserver-deployment

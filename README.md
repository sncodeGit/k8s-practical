# k8s-practical

## Pre-requests

1. Install docker
2. Install kubectl
3. Install minikube
4. Install helm

## Steps

1. [docker/docker.sh](docker/docker.sh)
2. [k8s/minukube.sh](k8s/minukube.sh)
3. [helm/helm.sh](k8s/helm.sh)
4. [operators/operators.sh](operators/operators.sh)

# Посмотрим, что сгенерировал helm
helm template pserver helm-chart/

# Задеплоим этот чарт
helm upgrade --install --namespace dev pserver helm-chart/

# Посмотрим, что он установлен
helm list -A

# Удалим
helm delete pserver -n dev

# Python 3 server example
from http.server import BaseHTTPRequestHandler, HTTPServer
import os

hostName = os.environ['HOST_NAME']
serverPort = int(os.environ['SERVER_PORT'])
bodyContent = os.environ['BODY_CONTENT']

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(
            bytes(
        """
            <html>
                <head>
                    <title>
                        Python site
                    </title>
                </head>
                <body>
                    <p>%s</p>
                </body>
            </html>
            """
            % bodyContent, "utf-8"))

if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
